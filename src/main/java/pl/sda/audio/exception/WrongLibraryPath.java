package pl.sda.audio.exception;

public class WrongLibraryPath extends RuntimeException {
    public WrongLibraryPath() {
        super("Wrong library path");
    }
}
