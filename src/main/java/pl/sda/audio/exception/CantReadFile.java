package pl.sda.audio.exception;

public class CantReadFile extends RuntimeException {
    public CantReadFile() {
        super("Can't read file");
    }
}
