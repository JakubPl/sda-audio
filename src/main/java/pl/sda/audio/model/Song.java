package pl.sda.audio.model;

public class Song {
    private final String title;
    private final String author;
    private final String lyrics;

    public Song(String title, String author, String lyrics) {
        this.title = title;
        this.author = author;
        this.lyrics = lyrics;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getLyrics() {
        return lyrics;
    }
}
