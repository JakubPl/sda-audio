package pl.sda.audio.service;

import pl.sda.audio.exception.CantReadFile;
import pl.sda.audio.exception.WrongLibraryPath;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class WorkingDirectoryService {
    private final Path libraryDirectory;

    public WorkingDirectoryService(Path libraryDirectory) {
        this.libraryDirectory = libraryDirectory;
    }

    public List<Path> getFiles() {
        try {
            return Files.list(libraryDirectory)
                    .filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".txt"))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new WrongLibraryPath();
        }
    }

    public String readLyrics(Path filename) {
        try {
            return String.join("", Files.readAllLines(libraryDirectory.resolve(filename)));
        } catch (IOException e) {
            throw new CantReadFile();
        }
    }
}
