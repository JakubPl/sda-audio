package pl.sda.audio;

import pl.sda.audio.model.Song;
import pl.sda.audio.service.WorkingDirectoryService;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Runner {
    private static final String SHOW_ALL_OPTION = "1";
    private static final String FILTER_BY_LYRICS_OPTION = "2";
    private static final String FILTER_BY_AUTHOR_OPTION = "3";
    private static final String FILTER_BY_SONGNAME_OPTION = "4";
    private static final String LYRICS_BY_AUTHOR_SONGNAME_OPTION = "5";
    private static final String EXIT_OPTION = "q";
    private Map<String, Song> authorTitleSongMap;
    private WorkingDirectoryService workingDirectoryService;
    private List<Song> songs;

    public Runner(WorkingDirectoryService workingDirectoryService) {
        this.workingDirectoryService = workingDirectoryService;
    }

    public void run() {
        System.out.println("Witaj użytkowniku!");
        songs = workingDirectoryService.getFiles()
                .stream()
                .map(this::mapToSong)
                .collect(Collectors.toList());
        authorTitleSongMap = songs.stream()
                .collect(Collectors.toMap(this::mapToAuthorTitleKey, v -> v));
        final Scanner scanner = new Scanner(System.in);
        String option;
        do {
            printMenu();
            option = scanner.nextLine();
            if (SHOW_ALL_OPTION.equals(option)) {
                printAllSongs();
            } else if (FILTER_BY_LYRICS_OPTION.equals(option)) {
                filterByLyrics(scanner);
            } else if (FILTER_BY_AUTHOR_OPTION.equals(option)) {
                filterByAuthor(scanner);
            } else if (FILTER_BY_SONGNAME_OPTION.equals(option)) {
                filterBySongName(scanner);
            } else if (LYRICS_BY_AUTHOR_SONGNAME_OPTION.equals(option)) {
                printLyrics(scanner);
            }
        } while (!EXIT_OPTION.equals(option));
    }

    private void printAllSongs() {
        songs.stream()
                .map(this::mapToAuthorTitle)
                .forEach(System.out::println);
    }

    private void filterBySongName(Scanner scanner) {
        System.out.println("Wprowadz nazwe utworu, ktorego szukasz lub jej fragment");
        final String songName = scanner.nextLine().toLowerCase();
        final List<String> authorsTitleList = songs.stream()
                .filter(song -> song.getTitle().toLowerCase().contains(songName))
                .map(this::mapToAuthorTitle)
                .collect(Collectors.toList());
        if (authorsTitleList.isEmpty()) {
            System.out.println("Nie znaleziono podanego utworu w bibliotece");
        } else {
            authorsTitleList.forEach(System.out::println);
        }
    }

    private void filterByAuthor(Scanner scanner) {
        System.out.println("Wprowadz jakiego autora utwory chcesz wyswietlic");
        final String desiredAuthor = scanner.nextLine();
        final List<String> authorsSongList = songs.stream()
                .filter(song -> song.getAuthor().equalsIgnoreCase(desiredAuthor))
                .map(this::mapToAuthorTitle)
                .collect(Collectors.toList());
        if (authorsSongList.isEmpty()) {
            System.out.println("Nie znaleziono podanego autora w bibliotece");
        } else {
            authorsSongList.forEach(System.out::println);
        }
    }

    private void printLyrics(Scanner scanner) {
        System.out.println("Wprowadz dokladny tytul");
        final String title = scanner.nextLine();
        System.out.println("Wprowadz dokladnego autora");
        final String author = scanner.nextLine();

        final String authorTitleKey = author + title;
        final String lyrics = Optional.ofNullable(authorTitleSongMap.get(authorTitleKey))
                .map(Song::getLyrics)
                .orElse("Nie znaleziono utworu");
        System.out.println(lyrics);
    }

    private void filterByLyrics(Scanner scanner) {
        System.out.println("Wprowadz jaki tekst powinna zawierac w sobie piosenka");
        final String lyricsPart = scanner.nextLine().toLowerCase();
        final List<String> songsWithGivenLyrics = songs.stream()
                .filter(song -> song.getLyrics().toLowerCase().contains(lyricsPart))
                .map(this::mapToAuthorTitle)
                .collect(Collectors.toList());
        if (songsWithGivenLyrics.isEmpty()) {
            System.out.printf("Nie znaleziono piosenki zawierajacej tekst %s%n", lyricsPart);
        } else {
            songsWithGivenLyrics.forEach(System.out::println);
        }
    }

    private void printMenu() {
        System.out.println("1. Wyswietl utwory w biblitece");
        System.out.println("2. Filtruj utwory po tekscie");
        System.out.println("3. Znajdz utworzy autora");
        System.out.println("4. Filtruj po nazwie utworu");
        System.out.println("5. Wyswietl tekst piosenki");
        System.out.println("q. Zakoncz prace");
    }

    private String mapToFilenameWithoutExtension(Path e) {
        final String fileNameWithExtension = e.getFileName().toString();
        final int lastDot = fileNameWithExtension.lastIndexOf('.');
        return fileNameWithExtension.substring(0, lastDot);
    }

    private String mapToAuthorTitleKey(Song song) {
        return song.getAuthor() + song.getTitle();
    }

    private String mapToAuthorTitle(Song song) {
        return new StringBuilder()
                .append(song.getAuthor())
                .append("-")
                .append(song.getTitle()).toString();
    }

    private Song mapToSong(Path filePath) {
        final String fileName = mapToFilenameWithoutExtension(filePath);
        final String[] authorAndSongName = fileName.split("-");
        final String author = authorAndSongName[0];
        final String songName = authorAndSongName[1];
        final String lyrics = workingDirectoryService.readLyrics(filePath);
        return new Song(songName, author, lyrics);
    }
}
