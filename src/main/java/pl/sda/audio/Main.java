package pl.sda.audio;

import pl.sda.audio.service.WorkingDirectoryService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("./application.properties"));
        //properties.setProperty("libraryPath", "./application.properties");
        //properties.store(new FileOutputStream("./application.properties"),"Change libraryPath");
        String libraryPath = properties.getProperty("libraryPath");
        Path path = Paths.get(libraryPath);
        Runner runner = new Runner(new WorkingDirectoryService(path));
        runner.run();
    }
}
